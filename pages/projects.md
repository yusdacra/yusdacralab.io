# Projects

## Projects I contribute(d) to

- [Harmony Protocol](https://github.com/harmony-development)
  - Various contributions to the reference server impl and the protocol itself.
  - Maintaining a server and client implementation, along with Rust ecosystem.

- [Veloren](https://gitlab.com/veloren/veloren) and it's [Book](https://gitlab.com/veloren/book)
  - Veloren is a multiplayer voxel RPG written in Rust.
  - My main contributions are for rendering and UI. Also maintaining build files for the Nix package manager and the Turkish translation. And added some guides to the book.

- [iced](https://github.com/hecrj/iced)
  - A cross-platform GUI library for Rust, inspired by Elm.

- [nixpkgs](https://github.com/NixOS/nixpkgs)
  - The Nix Packages collection for the [Nix package manager](https://github.com/NixOS/nix).
  - Package maintainer.

----

## Projects I maintain

- [rust-nix-templater](https://github.com/yusdacra/rust-nix-templater) and [nix-cargo-integration](https://github.com/yusdacra/nix-cargo-integration)
  - `nix-cargo-integration` allows easy and convenient integration of Cargo projects with Nix.
  - `rust-nix-templater` is a utility to create / generate Rust projects with Nix files and CI set up.

- [Harmony Rust SDK](https://github.com/harmony-development/harmony_rust_sdk)
  - Rust library that implements the Harmony protocol, and builds a client API on top of it.

- [scherzo](https://github.com/harmony-development/scherzo)
  - Harmony server implementation written in Rust, using sled as database and warp as HTTP server.

- [Crust](https://github.com/harmony-development/crust)
  - Harmony client written in Rust, using iced as GUI framework.

- [hrpc-rs](https://github.com/harmony-development/hrpc-rs)
  - [hrpc](https://github.com/harmony-development/hrpc) implementation in Rust.

- [linemd](https://github.com/yusdacra/linemd)
  - A no dependency, no std, simple and lightweight markdown parser and renderer (to HTML and SVG).

----

## Projects I used to work on

- [Icy Matrix](https://gitlab.com/yusdacra/icy_matrix)
  - A Matrix chat client built with Rust.
  - Uses `iced` for GUI.

- [Hakkero Kernel](https://gitlab.com/hakkero-os/hakkero)
  - Learning project to create a micro-kernel that runs on x86, arm and risc-v.
  - Current goal is to support webassembly and be able to run everything in ring 0, thanks to it's sandboxing features.

- [treeculler](https://gitlab.com/yusdacra/treeculler)
  - A Rust crate for working with frustum and occlusion culling.

- [bevy_networking_delivery](https://gitlab.com/yusdacra/bevy_prototype_networking_delivery)
  - A Rust crate for Bevy game engine that implements a simple `Delivery` based networking layer.

- [kIDE](https://gitlab.com/yusdacra/kide)
  - Opinionated configuration for kakoune text editor, focusing on an IDE-like experience.

- [FlutterEnhancements](https://github.com/yusdacra/FlutterEnhancements)
  - Basic Flutter support for Sublime Text 3.

- [Neonment](https://gitlab.com/yusdacra/neonment)
  - Neonment is a first person hero shooter. Name is subject to change.
